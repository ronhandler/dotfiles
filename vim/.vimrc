set nocompatible

" vim-plug plugins:
call plug#begin()
Plug 'cdelledonne/vim-cmake'
Plug 'cocopon/iceberg.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'preservim/nerdcommenter'
Plug 'ryanoasis/vim-devicons'
call plug#end()

" Disable swap and backup files.
set nobackup
set nowritebackup
set noswapfile

" Enable clipboard under Windows.
set clipboard=unnamed,autoselect
set mouse=a

" Session.vim settings.
set sessionoptions=globals,buffers,curdir,folds,tabpages

" Make searches ignore case-sensitive, and incremental.
set ignorecase
set smartcase
set incsearch
set hlsearch

set nowrap

set number
syntax on

" Attempt to remap the leader character to ","
let mapleader = ","
" Suggested by NERD Commenter.
filetype plugin on

" Set tab width to 4 characters.
set tabstop=4
set shiftwidth=4
set softtabstop=4
set smarttab
set expandtab
filetype indent plugin on
set autoindent
set smartindent

set textwidth=76

" Backspace and cursor keys wrap to previous/next line
set backspace=indent,eol,start whichwrap+=<,>,[,]

" Disable F1 for help.
map <F1> <nop>
imap <F1> <nop>

" Make errors search in open tabs, then open a new tab if not found the
" buffer.
set switchbuf=usetab,newtab

" Map a command to cd to current folder.
command! CdHere cd %:p:h
" Map a command to run a file.
let RunFile='./a.out'
command -nargs=* R
\ | exe '!' . RunFile . ' <args>'

set laststatus=2

" Airline configurations
let g:airline_theme='zenburn'
let g:airline_symbols_ascii = 1
"let g:airline_powerline_fonts = 1
let g:airline_symbols = {}
let g:airline_symbols.colnr = ' cn:'
let g:airline_symbols.linenr = ' ln:'
let g:airline_symbols.maxlinenr = ''
let g:airline_skip_empty_sections = 1
"let g:airline#extensions#tabline#enabled = 1
"let g:airline#extensions#tabline#fnamemod = ':t'
"let g:airline#extensions#tabline#buffer_idx_mode = 1
"nmap <leader>1 <Plug>AirlineSelectTab1
"nmap <leader>2 <Plug>AirlineSelectTab2
"nmap <leader>3 <Plug>AirlineSelectTab3
"nmap <leader>4 <Plug>AirlineSelectTab4
"nmap <leader>5 <Plug>AirlineSelectTab5
"nmap <leader>6 <Plug>AirlineSelectTab6
"nmap <leader>7 <Plug>AirlineSelectTab7
"nmap <leader>8 <Plug>AirlineSelectTab8
"nmap <leader>9 <Plug>AirlineSelectTab9
"nmap <leader>0 <Plug>AirlineSelectTab0
"nmap <leader>- <Plug>AirlineSelectPrevTab
"nmap <leader>= <Plug>AirlineSelectNextTab

colorscheme iceberg
set termguicolors
if &diff
    colorscheme default
    set notermguicolors
endif

" Needed for vim-devicons and nerd fonts
set encoding=UTF-8

